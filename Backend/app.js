const express = require("express");
const app = express();
const mongoose = require('mongoose');

// connecting to the Database
mongoose.connect("mongodb://localhost:27017/BulkUploading")
.then(() => {
    console.log('mongoose Connected');
})
.catch((error) => {
    console.error('Failed to connect to MongoDB:', error);
});


const tutorRoute = require('./routes/tutorRoute');
const studentRoute = require('./routes/studentRoute');


app.use('/tutors', tutorRoute);
app.use('/students', studentRoute);


app.listen(5000, function(){
    console.log('Server started on port 5000');
});
