const express = require('express');
const multer = require("multer");
const path = require('path');
const bodyParser = require('body-parser');
const tutorRoute = express.Router();
const tutorController = require('../controllers/tutorController');

// Middleware
tutorRoute.use(bodyParser.urlencoded({ extended: true }));
tutorRoute.use(express.static(path.resolve(__dirname, '../public'))); // Corrected path


// Custom error handling middleware
tutorRoute.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send('Something went wrong!');
});
// Multer configuration for file upload
const tutorstorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "./public/tutoruploads");
    },
    filename: (req, file, cb) => {
        cb(null, `${Date.now()}-${file.originalname}`);
    }
});

const tutorupload = multer({ storage: tutorstorage });



// Define route
tutorRoute.post('/importTutors', tutorupload.single('tutorfile'), tutorController.importTutors);

module.exports = tutorRoute;



