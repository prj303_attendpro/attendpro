const express = require('express');
const multer = require("multer");
const path = require('path');
const bodyParser = require('body-parser');
const studentController = require('../controllers/studentController');
const studentRoute = express.Router();

// Middleware
studentRoute.use(bodyParser.urlencoded({ extended: true }));
studentRoute.use(express.static(path.resolve(__dirname, '../public'))); // Corrected path


// Custom error handling middleware
studentRoute.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send('Something went wrong!');
});
// Multer configuration for file upload
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "./public/studentuploads");
    },
    filename: (req, file, cb) => {
        cb(null, `${Date.now()}-${file.originalname}`);
    }
});

const studentupload = multer({ storage: storage });



// Define route
studentRoute.post('/importStudents', studentupload.single('studentfile'), studentController.importStudents);

module.exports = studentRoute;



