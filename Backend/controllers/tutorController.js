const Tutor = require('../models/tutorModel');
const csv = require('csvtojson');
const bcrypt = require('bcryptjs');


const importTutors = async (req, res) => {
    try {
        // Parse CSV file and convert to JSON objects
        const tutorData = await csv().fromFile(req.file.path);

         // Validate each tutor data entry
       
         for (const data of tutorData) {
            const tutorValidationResult = isValidtutorData(data);
            if (tutorValidationResult !== true) {
               return res.status(400).json({ success: false, error: "Validation failed for one or more employees", validationErrors: [{ data, errors: tutorValidationResult }] });
            }
        }
       
        
        // Update existing documents or insert new ones
        await Promise.all(tutorData.map(async (data) => {
            data.password = await bcrypt.hash(data.password, 10);
            await Tutor.updateOne(
                { employeeid: data.employeeid }, // Query condition
                { $set: data }, // Data to update or insert
                { upsert: true } // Option to insert if document doesn't exist
            );
        }));
                res.send({status:200,success:true,msg:'tutor csv imported'})
     
       
    } catch (error) {
        res.send({ status:400, success: false, error: error.message });
    }
}

// Validate employee data
const isValidtutorData = (data) => {
    const { employeeid, employeename, gender, email, contactnumber, password } = data;
    
    const errors = {};

    // Validate employee number uniqueness
    // You can add your database checking logic here to ensure uniqueness
    // For now, let's assume no duplicate employee numbers are allowed in the CSV itself
    if (!employeeid || employeeid.length !== 4 || !/^\d{4}$/.test(employeeid)) {
        errors.employeeid = "Student number must be 4 digits long and contain only numbers";
    }


    // Validate email format
    const emailRegex = /^[a-zA-Z]{3,}\.gcit@rub\.edu\.bt$/;
    if (!emailRegex.test(email)) {
        errors.email = "Invalid email format";
    }

    // Validate contact number format
    const contactNumberRegex = /^(77|17)\d{6}$/;
    if (!contactNumberRegex.test(contactnumber)) {
        errors.contactnumber = "Invalid contact number format";
    }

    // Validate employee name length
    if (employeename.length <= 3) {
        errors.employeename = "employee name must be at least 4 characters long";
    }

    // Validate gender
    if (gender !== "male" && gender !== "female" ) {
        errors.gender = "Gender must be 'male' or 'female'  ";
    }

    // Validate other fields as required
    if (!password) {
        errors.password = "Password is required";
    }

    return Object.keys(errors).length === 0 ? true : errors;
};


module.exports = {
    importTutors
};