const Student = require('../models/studentModel');
const csv = require('csvtojson');
const bcrypt = require('bcryptjs');

const importStudents = async (req, res) => {
    try {
        // Parse CSV file and convert to JSON objects
        const studentData = await csv().fromFile(req.file.path);

         // Validate each student data entry
       
         for (const data of studentData) {
             const validationResult = isValidStudentData(data);
             if (validationResult !== true) {
                return res.status(400).json({ success: false, error: "Validation failed for one or more students", validationErrors: [{ data, errors: validationResult }] });
             }
         }
        
        
        // Update existing documents or insert new ones
        await Promise.all(studentData.map(async (data) => {
             // Encrypt password
             data.password = await bcrypt.hash(data.password, 10);

            await Student.updateOne(
                { studentnumber: data.studentnumber }, // Query condition
                { $set: data }, // Data to update or insert
                { upsert: true } // Option to insert if document doesn't exist
            );
        }));
        res.status(200).json({ success: true, msg: 'student csv imported' });
     
       
    } catch (error) {
        res.status(400).json({ success: false, error: error.message });
    }
};

// Validate student data
const isValidStudentData = (data) => {
    const { studentnumber, studentname, gender, email, contactnumber, password } = data;
    
    const errors = {};

    // Validate student number uniqueness
    // You can add your database checking logic here to ensure uniqueness
    // For now, let's assume no duplicate student numbers are allowed in the CSV itself
    if (!studentnumber || !/^12\d{6}$/.test(studentnumber)) {
        errors.studentnumber = "Student number must start with '12' followed by 6 other numbers";
    }

    // Validate email format
    const emailRegex = /^\d{8}\.gcit@rub\.edu\.bt$/;
    if (!emailRegex.test(email)) {
        errors.email = "Invalid email format";
    }

    // Validate contact number format
    const contactNumberRegex = /^(77|17)\d{6}$/;
    if (!contactNumberRegex.test(contactnumber)) {
        errors.contactnumber = "Invalid contact number format";
    }

    // Validate student name length
    if (studentname.length <= 3) {
        errors.studentname = "Student name must be at least 4 characters long";
    }

    // Validate gender
    if (gender !== "male" && gender !== "female" ) {
        errors.gender = "Gender must be 'male' or 'female'  ";
    }

    // Validate other fields as required
    if (!password) {
        errors.password = "Password is required";
    }

    return Object.keys(errors).length === 0 ? true : errors;
};


module.exports = {
    importStudents
};
