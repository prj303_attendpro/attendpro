


const mongoose = require('mongoose');




// defining Schema
const tutorsSchema = new mongoose.Schema({
    employeeid: {
        type: String,
        required: true,
        unique: true,

    },
    employeename: {
        type: String,
        required: true,
        
        
    },
    gender:{
        type :String  ,
        required:true,
    },
    email: {
        type: String,
        required: true,
        unique:true,
        
    },

    contactnumber:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true

    }
    
  
});

// creating a model
const TutorsRegisterCollection = mongoose.model('TutorsRegisterCollection', tutorsSchema);
module.exports = TutorsRegisterCollection;

