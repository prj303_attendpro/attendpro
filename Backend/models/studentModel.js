


const mongoose = require('mongoose');


// defining Schema
const studentSchema = new mongoose.Schema({
    studentnumber: {
        type: String,
        required: true,
        unique: true,

    },
    studentname: {
        type: String,
        required: true,
        
        
    },
    gender:{
        type :String  ,
        required:true,
    },
    email: {
        type: String,
        required: true,
        unique:true,
        
    },

    contactnumber:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true

    }
    
  
});

// creating a model
const StudentRegisterCollection = mongoose.model('StudentRegisterCollection', studentSchema);
module.exports = StudentRegisterCollection;

